import LoanRepository from './LoanRepository';

const repositories = {
    'loans': LoanRepository
}

export default {
    get: name => repositories[name]
};