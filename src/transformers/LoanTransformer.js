/* ============
 * Loan Transformer
 * ============
 *
 * The transformer for the loan.
 */

import Transformer from './Transformer';

export default class LoanTransformer extends Transformer {
  /**
   * Method used to transform a fetched loan.
   *
   * @param loan The fetched loan.
   *
   * @returns {Object} The transformed loan.
   */
  static fetch(loan) {
    return {
      id: loan.id,
      createdAt: loan.created_at,
      endDate: loan.end_date,
      amount: loan.amount,
      amountDebtPaid: loan.amount_debt_paid,
      repaymentFrequency: loan.repayment_frequency,
      period: loan.period,
      state: loan.state
    };
  }

  /**
   * Method used to transform a send loan.
   *
   * @param loan The loan to be send.
   *
   * @returns {Object} The transformed loan.
   */
  static send(loan) {
    return {
      amount: loan.amount,
      amount_debt_paid: 0,
      repayment_frequency: loan.repaymentFrequency,
      period: loan.period,
      state: 'processing'
    };
  }

  /**
   * Method used to transform a update loan.
   *
   * @param loan The loan to be update.
   *
   * @returns {Object} The transformed loan.
   */
  static update(loan) {
    return {
      amount_debt_paid: loan.amount,
      state: loan.state
    };
  }
}
