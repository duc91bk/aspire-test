import Vue from 'vue'
import App from './App.vue'
import moment from 'moment'

Vue.config.productionTip = false

Vue.filter('dateTime', function (value) {
  return moment(value).format('MM-DD-YYYY')
})

/* ============
 * Plugins
 * ============
 *
 * Import and bootstrap the plugins.
 */

import './plugins/vue-bootstrap';
import './plugins/vue-notification';
import { router } from './plugins/vue-router';
import store from './store';

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
