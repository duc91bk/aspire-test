/* ============
 * Mutation types for the post module
 * ============
 *
 * The mutation types that are available
 * on the post module.
 */

export const GET = 'GET';
export const GET_LOAN = 'GET_LOAN';
export const STATUS_LOADING = 'STATUS_LOADING';

export default {
    GET,
    GET_LOAN,
    STATUS_LOADING
};
