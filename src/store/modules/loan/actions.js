/* ============
 * Actions for the post module
 * ============
 *
 * The actions that are available on the
 * post module.
 */

import Transformer from '@/transformers/LoanTransformer';
import * as types from './mutation-types';

import Repository from '../../../repositories/RepositoryFactory';
const LoanRepository = Repository.get("loans");

export const get = ({ commit }) => {
  commit(types.STATUS_LOADING, true)
  LoanRepository.get().then((res) => {
    const list = res.data.map(loan => {
      return Transformer.fetch(loan)
    })

    commit(types.GET, list);
  }).catch(function (error) {
    console.log(error);
  })
  .then(function () {
    commit(types.STATUS_LOADING, false)
  })
};

export const post = ({ commit }, payload) => {
  commit(types.STATUS_LOADING, true)

  return LoanRepository.create(Transformer.send(payload))
};

export const update = ({ commit }, payload) => {
  commit(types.STATUS_LOADING, true)

  return LoanRepository.update(payload.id, Transformer.update(payload.formData))
};

export const remove = ({ commit }, id) => {
  commit(types.STATUS_LOADING, true)

  return LoanRepository.delete(id)
};

export default {
  get,
  post,
  update,
  remove
};
