/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */
import Cards from "../views/Cards/Index.vue";
import NotFound from "../views/404/Index.vue";

export default [
    {
        path: "/cards",
        name: "cards",
        component: Cards
    },
    {
        path: "/404",
        component: NotFound
    },
    {
        path: "/",
        redirect: "/cards"
    },

    {
        path: "/*",
        redirect: "/404"
    }
];
